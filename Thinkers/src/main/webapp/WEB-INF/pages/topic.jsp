<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Topic</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 <fmt:setLocale value="en_US" scope="session"/>
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>

			<div class="topic-info-container">		        
		        <ul> <li><h1>${topicInfo.name}</h1></li>
		        </ul>
		        <ul>
		            <li><h2>Author: ${topicInfo.creatorUser}</h2></li>
		            <li>public or private:  ${topicInfo.flag}</li>
		            <li><p align="justify">${topicInfo.description}</p></li>		            
		        </ul>
		        	Posts:        
		        <table border="1" style="width:100%">
		        <tr>
		            <th>Post name</th>
		            <th>User creator</th>
		            <th>Date</th>
		            <th>number of comments</th>
		            <th></th>
		            <th></th>
		        </tr>
		        <c:forEach items="${paginationResult}" var="postInfo">
		            <tr>
		                <td>                        
							<a href="${pageContext.request.contextPath}/post?id=${postInfo.id}">
							      ${postInfo.postName} </a>
		                </td>		                
		                <td>
		                   ${postInfo.postUser}
		                </td>
		                <td>
		                   <fmt:formatDate value="${postInfo.postDate}" pattern="dd-MM-yyyy HH:mm"/>
		                </td>
		                <td>comments</td>
		                <td>
			                <c:if test="${postInfo.postUser eq pageContext.request.userPrincipal.name}">	                        
	                       		<a href="${pageContext.request.contextPath}/postEdit?id=${postInfo.id}">Edit</a>
	                   		</c:if>		                  
		                </td>
		                <td>
		                	<c:if test="${postInfo.postUser eq pageContext.request.userPrincipal.name}">
		                		Invite
		                	</c:if>
		                </td>
		            </tr>
		        </c:forEach>
		    </table>
			    
			</div>			
			</td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>