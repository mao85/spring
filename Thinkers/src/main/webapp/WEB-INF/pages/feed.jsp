<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Account</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 <fmt:setLocale value="en_US" scope="session"/>

</script>
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>
		    <table style="width:100%">
		        <tr>
		            <th>Post name</th>
		            <th>Author</th>
		            <th>Date</th>
		            <th>Number of comments</th>
		        </tr>		        
		        <c:forEach items="${paginationResult.list}" var="postInfo">
		            <tr>
		                <td>
							<a href="${pageContext.request.contextPath}/post?id=${postInfo.id}">
								${postInfo.postName} </a>
		                </td>
		                <th>${postInfo.postUser}</th>
		            	<th><fmt:formatDate value="${postInfo.postDate}" pattern="dd-MM-yyyy HH:mm"/></th>
		            	<th>comments</th>		                
		            </tr>
		        </c:forEach>		        
		    </table>
			    <c:if test="${paginationResult.totalPages > 1}">
			        <div class="page-title">
			           <c:forEach items="${paginationResult.navigationPages}" var = "page">
			               <c:if test="${page != -1 }">
			                 <a href="feed?page=${page}" class="nav-item">${page}</a>
			               </c:if>
			               <c:if test="${page == -1 }">
			                 <span class="nav-item"> ... </span>
			               </c:if>
			           </c:forEach>		            
			        </div>
	    		</c:if>
			 </td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>