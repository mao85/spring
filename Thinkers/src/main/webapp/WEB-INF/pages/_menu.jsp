<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>   
 
 
<div class="menu-container">
  <table>
	  <tr>
	  	<td> <a href="${pageContext.request.contextPath}/">Home</a> </td>  
	  </tr>
	 
	  <tr>
		  <td><security:authorize  access="hasRole('ROLE_USER')">
		  <a href="${pageContext.request.contextPath}/registration?name=${pageContext.request.userPrincipal.name}">
		         Edit account
		     </a>
		  </security:authorize>
		  </td>  
	  </tr>
	  <tr>
		  <td><security:authorize  access="hasRole('ROLE_USER')">
		  <a href="${pageContext.request.contextPath}/topicList">
		         Topics
		   </a>
		  </security:authorize>
		  </td>  
	  </tr>
	  <tr>
		  <td><security:authorize  access="hasRole('ROLE_USER')">
		  <a href="${pageContext.request.contextPath}/topicNew">
		         Create topics
		   </a>
		  </security:authorize>
		  </td>  
	  </tr>
	  <tr>
		  <td><security:authorize  access="hasRole('ROLE_USER')">
		  <a href="${pageContext.request.contextPath}/search">
		         Search
		   </a>
		  </security:authorize>
		  </td>  
	  </tr>	
	  <tr>
		  <td><security:authorize  access="hasRole('ROLE_USER')">
		  <a href="${pageContext.request.contextPath}/feed">
		         Feed
		   </a>
		  </security:authorize>
		  </td>  
	  </tr>	
	   <tr>
		  <td><security:authorize  access="hasRole('ROLE_USER')">
		  	<a href="${pageContext.request.contextPath}/notificationMy">
	 		 	Notification
		 	</a>
		  </security:authorize>
		  </td>  
	   </tr>	
  </table>
 
   
</div>