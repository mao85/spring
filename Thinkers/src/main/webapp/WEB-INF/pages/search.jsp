<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Account</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 <fmt:setLocale value="en_US" scope="session"/>

</script>
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>
		    <table style="width:100%">
		        <tr>
		            <th>
		            <form action="${pageContext.request.contextPath}/searchTest" method="get">
						Topic name:	<input type = "text" name="topicName" />						
						<input type="submit" value="search" />		            	
		            </form>	    		            
		            </th>
		        </tr>		        
		        <c:forEach items="${paginationResult.list}" var="topicInfo">
		            <tr>
		                <td>
							<c:if test="${topicInfo.creatorUser eq pageContext.request.userPrincipal.name}">	                        
								<a href="${pageContext.request.contextPath}/topicMy?id=${topicInfo.id}">
							      ${topicInfo.topicName} </a>
							</c:if>
						
							<c:if test="${topicInfo.creatorUser ne pageContext.request.userPrincipal.name}">
								<a href="${pageContext.request.contextPath}/topic?id=${topicInfo.id}">
									${topicInfo.topicName} </a>
							</c:if>
		                </td>
		                <td>
		                	<c:if test="${topicInfo.creatorUser eq pageContext.request.userPrincipal.name}">	                        							
							      My topic
							</c:if>
						
							<c:if test="${topicInfo.creatorUser ne pageContext.request.userPrincipal.name}">
								<c:if test="${topicInfo.userName eq pageContext.request.userPrincipal.name}">
									<a href="${pageContext.request.contextPath}/LeaveTopic?cod=${topicInfo.topicName}">Leave </a>
								</c:if>
								<c:if test="${topicInfo.userName ne pageContext.request.userPrincipal.name}">
									<a href="${pageContext.request.contextPath}/addTopic?nameTopic=${topicInfo.topicName}"> Login </a>
								</c:if>
							</c:if>		                
		                </td>			                
		            </tr>
		        </c:forEach>
		        <c:forEach items="${paginationResultList}" var="topicInfoList">
		            <tr>
		                <td>
							<c:if test="${topicInfoList.creatorUser eq pageContext.request.userPrincipal.name}">	                        
								<a href="${pageContext.request.contextPath}/topicMy?id=${topicInfoList.id}">
							      ${topicInfoList.topicName} </a>
							</c:if>
						
							<c:if test="${topicInfoList.creatorUser ne pageContext.request.userPrincipal.name}">
								<a href="${pageContext.request.contextPath}/topic?id=${topicInfoList.id}">
									${topicInfoList.topicName} </a>
							</c:if>
		                </td>
		                <td>
		                	<c:if test="${topicInfoList.creatorUser eq pageContext.request.userPrincipal.name}">	                        							
							      My topic
							</c:if>
						
							<c:if test="${topicInfoList.creatorUser ne pageContext.request.userPrincipal.name}">
								<c:if test="${topicInfoList.userName eq pageContext.request.userPrincipal.name}">
									<a href="${pageContext.request.contextPath}/LeaveTopic?cod=${topicInfoList.topicName}">Leave </a>
								</c:if>
								<c:if test="${topicInfoList.userName ne pageContext.request.userPrincipal.name}">
									<a href="${pageContext.request.contextPath}/addTopic?nameTopic=${topicInfoList.topicName}"> Login </a>
								</c:if>
							</c:if>		                
		                </td>			                
		            </tr>
		        </c:forEach>
		    </table>
			    <c:if test="${paginationResult.totalPages > 1}">
			        <div class="page-title">
			           <c:forEach items="${paginationResult.navigationPages}" var = "page">
			               <c:if test="${page != -1 }">
			                 <a href="search?page=${page}" class="nav-item">${page}</a>
			               </c:if>
			               <c:if test="${page == -1 }">
			                 <span class="nav-item"> ... </span>
			               </c:if>
			           </c:forEach>		            
			        </div>
	    		</c:if>
			 </td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>