<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Topic</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 <fmt:setLocale value="en_US" scope="session"/>
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>

			<div class="topic-info-container">		        
		        <ul> <li><h1>${postInfo.postName}</h1></li>
		        </ul>
		        <ul>
		            <li><h2>Author: ${postInfo.postUser}</h2></li>
		            <li><h2> <fmt:formatDate value="${postInfo.postDate}" pattern="dd-MM-yyyy HH:mm"/></h2></li>
		            <li><p align="justify">${postInfo.postDescription}</p></li>		            
		        </ul>
			</div>			
			</td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>