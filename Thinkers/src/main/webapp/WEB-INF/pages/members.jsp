<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Account</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 <fmt:setLocale value="en_US" scope="session"/>

</script>
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>
			
		    <table style="width:100%">
		        <tr>
		            <th>${notification.nameTopic}</th>
		            <th>++++***++${nameTopic} <input name = "nameTopic" value="${nameTopic}"/> </th>
		            
		        </tr>		        
		        <c:forEach items="${listMember}" var="listnotmember">
		            <tr>
		            	<td>${listnotmember.user}  </td>
		            	
		            	<td> 
		            	
		            	<a href="${pageContext.request.contextPath}/addInvite?userName=${listnotmember.user}">Invite </a></td>	                	                
		            </tr>
		        </c:forEach>
		        </table>
		    	        
			 </td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>