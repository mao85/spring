<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Topic</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 	<c:if test="${not empty errorMessage }">
      <div class="error-message">
          ${errorMessage}
      </div>
    </c:if>
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:20%"><jsp:include page="_menu.jsp" /></td>
			<td>
				<form:form modelAttribute="postInfo" method="POST" >
			        <table style="text-align:left;">				        
			        	
		                <tr>
			                <td>Date *</td>
			               
		                    <td>  ${postInfo.postDate}</td>
		                </tr>
			        	<tr>
			                <td>User creator *</td>
			                <td> ${postInfo.postUser}</td>		                
			            </tr>
			            <tr>
			                <td>Post name *</td>
		                    <td> <form:input path="postName" /></td>
		                </tr>		 			
			            <tr>
			                <td>Description *</td>
			                <td><form:textarea cols="60" rows="8" path="postDescription" /></td>		                
			            </tr>			            
	  	                <tr>
			                <td>&nbsp;</td>
			                <td><input type="submit" value="Submit" /> <input type="reset"
			                    value="Reset" /></td>
			            </tr>
			        </table>
			    </form:form>			
			</td>
		</tr>		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>