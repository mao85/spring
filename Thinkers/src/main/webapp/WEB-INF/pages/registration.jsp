<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Account</title>
 
<link rel="stylesheet" type="text/css" href="styles.css">
 
</head>
<body>
 	
    <c:if test="${not empty errorMessage }">
      <div class="error-message">
          ${errorMessage}
      </div>
    </c:if>
    
    <div class="onli">  
    <table  width=100%>
		<tr>
			<td colspan=2>
				<jsp:include page="_header.jsp" />
			</td>
		</tr>
		<tr>
			<td style="width:15%"><jsp:include page="_menu.jsp" /></td>
			<td>
				 

<form:form modelAttribute="accountForm" method="POST" enctype="multipart/form-data">
        <table style="text-align:left;">
            <tr>
                <td>Name *</td>
                <td style="color:red;">
                   <c:if test="${not empty accountForm.name}">
                        <form:hidden path="name"/>
                        ${accountForm.name}
                   </c:if>
                   <c:if test="${empty accountForm.name}">
                        <form:input path="name" />
                        <form:hidden path="newAccount" />
                   </c:if>
                </td>
                <td><form:errors path="name" class="error-message" /></td>
            </tr>
 
            <tr>
                <td>Password *</td>
                <td><form:input path="password" type="password" /></td>
                <td><form:errors path="password" class="error-message" /></td>
            </tr>
 
            <tr>
                <td>Password2 *</td>
                <td><form:input path="password2" type="password" /></td>
                <td><form:errors path="password2" class="error-message" /></td>
            </tr>
            <tr>
                <td>Image</td>
                <td>
                <img src="${pageContext.request.contextPath}/image?name=${accountForm.name}" width="100"/></td>
                <td> </td>
            </tr>
            <tr>
                <td>Upload Image</td>
                <td><form:input type="file" path="fileData"/></td>
                <td> </td>
            </tr>
  
 
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="Submit" /> <input type="reset"
                    value="Reset" /></td>
            </tr>
        </table>
    </form:form>
			</td>
		</tr>
		
	</table>  
 	</div> 
 	<jsp:include page="_footer.jsp" />
</body>
</html>