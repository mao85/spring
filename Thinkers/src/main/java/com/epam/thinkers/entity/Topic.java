package com.epam.thinkers.entity;

import java.io.Serializable;
import java.util.UUID;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "topics")
public class Topic implements Serializable {
 
    private static final long serialVersionUID = -2576670215015463100L;
 
    private String id;
    private String nameTopic;
    
    private String description;
   // private byte[] photoTopic;
    private String creatorUser;
    private boolean flag;
    
    @Id
    @Column(name = "ID", length = 50)
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "User_Name")
	public String getCreatorUser() {
		return creatorUser;
	}
	public void setCreatorUser(String creatorUser) {
		this.creatorUser = creatorUser;
	}
	
	@Column(name = "Topic_Name", nullable = false)
	public String getNameTopic() {
		return nameTopic;
	}
	public void setNameTopic(String nameTopic) {
		this.nameTopic = nameTopic;
	}
	
	@Column(name = "Flag")
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	@Column(name = "Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	/*@Lob
	@Column(name = "Photo_Topic", length = Integer.MAX_VALUE, nullable = true)
	public byte[] getPhotoTopic() {
		return photoTopic;
	}
	public void setPhotoTopic(byte[] photoTopic) {
		this.photoTopic = photoTopic;
	}
	*/
}
