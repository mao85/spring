package com.epam.thinkers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "invite")
public class Notification implements Serializable {
 
    private static final long serialVersionUID = -6892586655965881969L;
 
    private String id;
    private String inviter;
    private String user;
    private String nameTopic;
    private Date dateCreate;
	public Notification() {

	}
	
	@Id
    @Column(name = "ID")
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "Inviter")
	public String getInviter() {
		return inviter;
	}
	public void setInviter(String inviter) {
		this.inviter = inviter;
	}
	@Column(name = "User")
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	@Column(name = "Topic_Name")
	public String getNameTopic() {
		return nameTopic;
	}
	public void setNameTopic(String nameTopic) {
		this.nameTopic = nameTopic;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Data")
	public Date getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}
	
}
