package com.epam.thinkers.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import com.epam.thinkers.dao.AccountDAO;
import com.epam.thinkers.dao.PostDAO;
import com.epam.thinkers.dao.TopicDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Post;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.FullTopicInfo;
import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.PostInfo;
import com.epam.thinkers.model.TopicDetailInfo;
import com.epam.thinkers.model.TopicInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class TopicDaoImpl implements TopicDAO{

	@Autowired
    private SessionFactory sessionFactory;
	
	@Autowired
    private AccountDAO accountDAO;
	
	@Autowired
    private PostDAO postDAO;
	
	public void saveTopic(TopicInfo topicinfo) {
		String code = topicinfo.getId();
		 
        Topic topic = null;
 
        boolean isNew = false;
        if (code != null) {
        	topic = this.findTopic(code);
        }
        if (topic == null) {        	
            isNew = true;
            topic = new Topic();
            //AccountInfo accountInfo = topicinfo.getAccountInfo();
            topic.setId(topicinfo.getId());
            topic.setCreatorUser(topicinfo.getCreatorUser());            
        }  
        topic.setNameTopic(topicinfo.getName());
        topic.setDescription(topicinfo.getDescription());
        topic.setFlag(topicinfo.isFlag());   
        
        this.sessionFactory.getCurrentSession().persist(topic);
        
        List<AccountInfo> userLines  = topicinfo.getUserLines();
        for (AccountInfo line : userLines) {
        	TopicDetails detail = new TopicDetails();
            detail.setId(UUID.randomUUID().toString());
            detail.setTopic(topic);
            
            String nameMember = line.getName();
            Account account = this.accountDAO.findAccount(nameMember);
            detail.setUser(account);
            
            this.sessionFactory.getCurrentSession().persist(detail);
        }
        
        List<PostInfo> postLines  = topicinfo.getPostLines();
        for (PostInfo line : postLines) {
        	Post post = new Post();
        	post.setId(line.getId());
        	post.setTopic(topic);
            String namePost = line.getPostName();
            String description = line.getTopicDescription();
            Date datePost = line.getPostDate();
            String userPost = line.getPostUser();
            post.setNamePost(namePost);
            post.setDescription(description);
            post.setDateCreate(datePost);
            post.setCreatorUser(userPost);
                        
            this.sessionFactory.getCurrentSession().persist(post);
        }
            
            /*if (isNew) {
            this.sessionFactory.getCurrentSession().persist(topic);            
        }  
       
       
       
        this.sessionFactory.getCurrentSession().flush();
        //this.sessionFactory.getCurrentSession().save(topic);
        
        */
        
	}
	   
	public PaginationResult<TopicInfo> listTopicInfo(int page, int maxResult, int maxNavigationPage) {
		String sql = "Select new " + TopicInfo.class.getName()//
                + "(top.id, top.nameTopic, top.description, top.creatorUser, top.flag) " + " from "
                + Topic.class.getName() + " top "//
                + " order by top.nameTopic asc";
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
 
        return new PaginationResult<TopicInfo>(query, page, maxResult, maxNavigationPage);
	}
	
	public Topic findTopic(String topicId) {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Topic.class);
        crit.add(Restrictions.eq("id", topicId));
        return (Topic) crit.uniqueResult();
    }

	public TopicInfo getTopicInfo(String topicId) {
		Topic topic = this.findTopic(topicId);
        if (topic == null) {
            return null;
        }
        return new TopicInfo(topic.getId(), topic.getNameTopic(), topic.getDescription(), topic.getCreatorUser(), topic.isFlag());
	}
	
	public List<TopicDetailInfo> listTopicDetailInfos(String topicId) {
		String sql = "Select new " + TopicDetailInfo.class.getName() //
                + "(d.id, d.user.userName, d.topic.nameTopic, d.topic.flag) "//
                + " from " + TopicDetails.class.getName() + " d "//
                + " where d.topic.id = :topicId "
                + " order by d.topic.nameTopic asc";
 
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("topicId", topicId);
 
        return query.list();
	}
	public List<AccountInfo> listMember(String nameTopic) {
		String sql = "Select new " + AccountInfo.class.getName() //
                + "(d.user.userName) "//
                + " from " + TopicDetails.class.getName() + " d "//
                + " where d.topic.nameTopic = :nameTopic"
                + " group by d.user.userName"
                + " order by d.user.userName asc";
 
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("nameTopic", nameTopic);
 
        return query.list();
	}
	
	public List<TopicDetailInfo> listTopicDetailUserByNeme(String name) {
		String sql = "Select new " + TopicDetailInfo.class.getName() //
				+ "( d.topic.id, d.user.userName, d.topic.nameTopic, d.topic.description, d.topic.creatorUser) "//
                + " from " + TopicDetails.class.getName() + " d"//
                + " where d.user.userName = :name";
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("name", name);
 
        return query.list();
	}
	
	public PaginationResult<TopicDetailInfo> listTopicDetailUser(int page, int maxResult, int maxNavigationPage, String name) {
		String sql = "Select new " + TopicDetailInfo.class.getName() //
				+ "( d.topic.id, d.user.userName, d.topic.nameTopic, d.topic.description, d.topic.creatorUser) "//
                + " from " + TopicDetails.class.getName() + " d"//
                + " where d.user.userName = :name";
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("name", name);
 
        return new PaginationResult<TopicDetailInfo>(query, page, maxResult, maxNavigationPage);
	}
//++++++++++++++++++++++++++++++++++
	public List<TopicDetails> listTopicDetails() {
		
		return this.sessionFactory.getCurrentSession().createQuery("from TopicDetails").list();
	}

	public void remove(TopicDetails name) {
		sessionFactory.getCurrentSession().delete(name);
	}
	
	public Topic findTopicForName(String nameTopic) {       
        Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Topic.class);
        crit.add(Restrictions.eq("nameTopic", nameTopic));
        return (Topic) crit.uniqueResult();
    }
	
	public Topic findTopicByUser(String creatorUser) {       
        Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Topic.class);
        crit.add(Restrictions.eq("creatorUser", creatorUser));
        return (Topic) crit.uniqueResult();
    }
	
	public TopicDetailInfo findTopicDetailInfo(String nameTopic, String userName) {       
        Topic topic = this.findTopicForName(nameTopic);
		Account user = accountDAO.findAccount(userName);
        if (topic == null || user == null) {
            return null;
        }
        return new TopicDetailInfo( topic.getId(), user.getUserName(), topic.getNameTopic(), topic.getDescription(), topic.getCreatorUser());
    }
	
	
	public void addDetails(TopicDetailInfo topicinfo) {
		//String idTopic = topicinfo.getId();
		String userName = topicinfo.getUserName();
		String nameTopic = topicinfo.getTopicName();
		
		Topic topic = this.findTopicForName(nameTopic);
		Account user = accountDAO.findAccount(userName);
		
		TopicDetails topicDetails = new TopicDetails();
        
		topicDetails.setId(UUID.randomUUID().toString());        
        topicDetails.setTopic(topic);                     
        topicDetails.setUser(user);      
        
        this.sessionFactory.getCurrentSession().persist(topicDetails);
	}

//---------------------------------------------------
	public List<Topic> getAll() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from  Topic");
		return  query.list();
	}


	public Topic get(String id) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Topic.class);
        crit.add(Restrictions.eq("id", id));
        return (Topic) crit.uniqueResult();
	}


	public TopicInfo getInfo(String topicId) {
		Topic topic = this.get(topicId);
        if (topic == null) {
            return null;
        }
        return new TopicInfo(topic.getId(), topic.getNameTopic(), topic.getDescription(), topic.getCreatorUser(), topic.isFlag());
	}

	public TopicDetailInfo getDetInfo(String topicId) {
		Topic topic = this.get(topicId);
        if (topic == null) {
            return null;
        }
        return new TopicDetailInfo(topic.getId(), topic.getNameTopic(), topic.getDescription(), topic.getCreatorUser(), topic.isFlag());
	}

	public void add(Topic topic) {
		Session session = sessionFactory.getCurrentSession();
		session.save(topic);
	}


	public void delete(String id) {
		Session session = sessionFactory.getCurrentSession();
		Topic topic = (Topic) session.get(Topic.class, id);
		session.delete(topic);
	}
	

	public void edit(TopicInfo topicInfo) {
		//Session session = sessionFactory.getCurrentSession();
		Topic existingTopic = (Topic) this.sessionFactory.getCurrentSession().get(Topic.class, topicInfo.getId());
		existingTopic.setNameTopic(topicInfo.getName());
		existingTopic.setDescription(topicInfo.getDescription());
		existingTopic.setCreatorUser(existingTopic.getCreatorUser());
		existingTopic.setFlag(topicInfo.isFlag());
		//this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().save(existingTopic);
	}
	
	
	
	public PaginationResult<TopicDetailInfo> listTopicSearch(int page, int maxResult, int maxNavigationPage, String name) {
		String sql = "Select new " + TopicDetailInfo.class.getName() //
				+ "( d.topic.id, d.user.userName, d.topic.nameTopic, d.topic.description, d.topic.creatorUser) "//
                + " from " + TopicDetails.class.getName() + " d, "+Topic.class.getName()+ " t"//
                + " where d.user.userName = :name and t.creatorUser = :name or d.topic.flag = 1"
                + " group by d.topic.nameTopic order by d.topic.nameTopic asc";       
 
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("name", name);
        
        return new PaginationResult<TopicDetailInfo>(query, page, maxResult, maxNavigationPage);
	}
	
	public List<TopicDetailInfo> listTopicSearchTest(String name, String topicName) {
		String sql = "Select new " + TopicDetailInfo.class.getName() //
				+ "( d.topic.id, d.user.userName, d.topic.nameTopic, d.topic.description, d.topic.creatorUser) "//
                + " from " + TopicDetails.class.getName() + " d, "+Topic.class.getName()+ " t"//
                + " where ((d.user.userName = :name and t.creatorUser = :name) or (d.topic.flag = 1)) and lower(d.topic.nameTopic) like :topicName"
                + " group by d.topic.nameTopic order by d.topic.nameTopic asc";       
 
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("name", name);
        query.setParameter("topicName",'%'+ topicName.toLowerCase()+'%');
        
        return query.list();
	}
	
	public Topic getInfoByUser(String topicUser) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Topic.class);
        crit.add(Restrictions.eq("creatorUser", topicUser));
        return (Topic) crit.uniqueResult();
	}

	public TopicInfo findTopicInfoByUser(String topicUser) {
		Topic topic = this.getInfoByUser(topicUser);
        if (topic == null) {
            return null;
        }
        return new TopicInfo(topic.getId(), topic.getNameTopic(), topic.getDescription(), topic.getCreatorUser(), topic.isFlag());
	}
	
	public TopicInfo findTopicInfoByName(String topicName) {
		Topic topic = this.findTopicForName(topicName);
        if (topic == null) {
            return null;
        }
        return new TopicInfo(topic.getId(), topic.getNameTopic(), topic.getDescription(), topic.getCreatorUser(), topic.isFlag());
	}
	
	
}
