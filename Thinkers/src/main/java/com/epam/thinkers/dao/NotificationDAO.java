package com.epam.thinkers.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.epam.thinkers.entity.Notification;
import com.epam.thinkers.model.NotificationInfo;

public interface NotificationDAO {
	
public List<Notification> listNotification();

	public void remove(Notification name);
	public List<NotificationInfo> listNotificationInfo(String name);
	public Notification findNotificationByTopicNeme(String nameTopic);
	/*public void addDetails(String id) {
		Notification notification = this.findNotificationByID(id);
		List<Notification> list = this.listTopicDetails();
		list.add(notification);        
		this.sessionFactory.getCurrentSession().persist(notification);
	}
    */
	public Notification findNotificationByID(String id);

	public List<NotificationInfo> listmember(String topicName);
	public List<NotificationInfo> listaccount();
	
	public void saveNotification(NotificationInfo notification);
	public void updateNotification(NotificationInfo notificationInfo);
	public List<NotificationInfo> listNotificationInfoMy(String name);
	
	public NotificationInfo getNotificationInfo(String inviter, String nameTopic);
}
