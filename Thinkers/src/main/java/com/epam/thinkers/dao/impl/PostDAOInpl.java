package com.epam.thinkers.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.thinkers.dao.AccountDAO;
import com.epam.thinkers.dao.PostDAO;
import com.epam.thinkers.dao.TopicDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Post;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.PostDetaisInfo;
import com.epam.thinkers.model.PostInfo;
import com.epam.thinkers.model.TopicDetailInfo;
import com.epam.thinkers.model.TopicInfo;

public class PostDAOInpl implements PostDAO {

	@Autowired
    private SessionFactory sessionFactory;
	
	@Autowired
    private TopicDAO topicDAO;
	
	
	public void savePost(PostInfo postInfo){
		
		String code = postInfo.getId();
		String topicID = postInfo.getTopicID(); 
        Post post = null;
 
        //boolean isNew = false;
        if (code != null) {
        	post = this.findPost(code);
        }
        if (post == null) {        	
        //     isNew = true;
            post = new Post();
            //AccountInfo accountInfo = topicinfo.getAccountInfo();
            post.setId(code);
            post.setCreatorUser(postInfo.getPostUser()); 
            post.setDateCreate(postInfo.getPostDate());
        }  
        post.setNamePost(postInfo.getPostName());
        post.setDescription(postInfo.getPostDescription());          
        Topic topic = topicDAO.findTopic(topicID);
        post.setTopic(topic);
        
        this.sessionFactory.getCurrentSession().persist(post);
                           
	}
	
	
	private Post findPost(String postId) {
		Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Post.class);
        crit.add(Restrictions.eq("id", postId));
        return (Post) crit.uniqueResult();
	}
	
	public PostInfo getPostInfo(String postId) {
		Post post = this.findPost(postId);
        if (post == null) {
            return null;
        }
        return new PostInfo(post.getId(), post.getNamePost(), post.getDateCreate(), post.getCreatorUser(), post.getDescription(), post.getTopic().getNameTopic(), post.getTopic().getId());
	}
	
	private Post findPostByUser(String creatorUser) {
		Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Post.class);
        crit.add(Restrictions.eq("creatorUser", creatorUser));
        return (Post) crit.uniqueResult();
	}
	public PostInfo getPostInfoByUser(String creatorUser) {
		Post post = this.findPostByUser(creatorUser);
        if (post == null) {
            return null;
        }
        return new PostInfo(post.getId(), post.getNamePost(), post.getDateCreate(), post.getCreatorUser(), post.getDescription(), post.getTopic().getNameTopic(), post.getTopic().getId());
	}
	
    
	public List<PostInfo> listPostByTopic(String topicID){
		String sql = "Select new " + PostInfo.class.getName() //
				+ "(p.id, p.namePost, p.dateCreate, p.creatorUser, p.description, p.topic.nameTopic, p.topic.id) "//
                + " from " + Post.class.getName() + " p"//
                + " where p.topic.id = :topicID";
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("topicID", topicID);
 
        return query.list();
	}
	
	public List<PostDetaisInfo> listPostDetailsByTopic(String topicID){
		String sql = "Select new " + PostDetaisInfo.class.getName() //
				+ "(p.id, p.namePost, p.dateCreate, p.creatorUser, p.description, p.topic.nameTopic, p.topic.id) "//
                + " from " + Post.class.getName() + " p"//
                + " where p.topic.id = :topicID";
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("topicID", topicID);
 
        return query.list();
	}
	
	public void edit(PostInfo postInfo){
		//String topicId = postInfo.getTopicID();
		//String topicName = postInfo.getTopicName();
		//Topic topic = this.topicDAO.findTopicForName(topicName);
		Post existingPost = (Post) this.sessionFactory.getCurrentSession().get(Post.class, postInfo.getId());
		existingPost.setNamePost(postInfo.getPostName());
		existingPost.setDescription(postInfo.getPostDescription());
		existingPost.setCreatorUser(existingPost.getCreatorUser());
		existingPost.setDateCreate(existingPost.getDateCreate());
		//existingPost.setTopic(topic);
		//this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().save(existingPost);
	}
	
	public PaginationResult<PostInfo> listPostByUser(int page, int maxResult, int maxNavigationPage, String userName){
		String sql = "Select new " + PostInfo.class.getName() //
				+ "(p.id, p.namePost, p.dateCreate, p.creatorUser, p.description, p.topic.nameTopic, p.topic.id)"//
                + " from " + Post.class.getName() + " p, "//
                + Topic.class.getName()+" t, "
                + TopicDetails.class.getName()+" d where (p.topic.id = t.id and p.topic.id = d.topic.id and d.user.userName =:userName)"
                		+ "order by p.dateCreate asc";
	
		Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("userName", userName);
 
        return new PaginationResult<PostInfo>(query, page, maxResult, maxNavigationPage);
	}
	
}
