package com.epam.thinkers.dao;

import java.util.List;

import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.FullTopicInfo;
import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.TopicDetailInfo;
import com.epam.thinkers.model.TopicInfo;

public interface TopicDAO {

	public List<TopicDetails> listTopicDetails();
	public void remove(TopicDetails name);
	public TopicInfo getTopicInfo(String topicId);
	public PaginationResult<TopicInfo> listTopicInfo(int page, int maxResult, int maxNavigationPage); 
	public PaginationResult<TopicDetailInfo> listTopicDetailUser(int page, int maxResult, int maxNavigationPage, String name);   
	public void saveTopic(TopicInfo Topic);	
	public Topic findTopicForName(String nameTopic);
	public TopicDetailInfo findTopicDetailInfo(String nameTopic, String userName);
	public void addDetails(TopicDetailInfo topic);
	
	public List<TopicDetailInfo> listTopicDetailInfos(String name); 
	
	public void edit(TopicInfo topicInfo);
	public TopicInfo getInfo(String topicId);
	
	public Topic findTopicByUser(String creatorUser);
	public PaginationResult<TopicDetailInfo> listTopicSearch(int page, int maxResult, int maxNavigationPage, String name);
	
	public List<TopicDetailInfo> listTopicSearchTest(String name, String topicName);
	public List<TopicDetailInfo> listTopicDetailUserByNeme(String name);
	public TopicInfo findTopicInfoByName(String topicName);
	public TopicDetailInfo getDetInfo(String topicId);
	//public void saveTopic(FullTopicInfo fulltopicInfo);
	 
   // public void removeDetails(String topicId);
   
  //  public Topic find(String topicName);
  //  public Topic save(Topic topic);
	
	public List<Topic> getAll();
	public Topic get( String id );
	public void add(Topic topic);
	public void delete(String id);
	//public void edit(Topic topic);
	public Topic findTopic(String topicID);
	public TopicInfo findTopicInfoByUser(String name);
	public List<AccountInfo> listMember(String nameTopic);
 
}
