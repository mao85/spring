package com.epam.thinkers.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.thinkers.dao.NotificationDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Notification;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.NotificationInfo;
import com.epam.thinkers.model.TopicDetailInfo;
import com.epam.thinkers.model.TopicInfo;

public class NotificationDAOImpl implements NotificationDAO {

	@Autowired
    private SessionFactory sessionFactory;
	
	public List<NotificationInfo> listNotificationInfo(String name) {
		String sql = "Select new " + NotificationInfo.class.getName() //
				+ "(n.id, n.inviter, n.user, n.nameTopic, n.dateCreate) "//
                + " from " + Notification.class.getName() + " n"//
                + " where (n.user = :name) order by n.dateCreate asc";       
		
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("name", name);
        
        return query.list();
	}
	
	public List<NotificationInfo> listNotificationInfoMy(String name) {
		String sql = "Select new " + NotificationInfo.class.getName() //
				+ "(n.id, n.inviter, n.user, n.nameTopic, n.dateCreate) "//
                + " from " + Notification.class.getName() + " n"//
                + " where (n.user = :name and n.inviter !=:name) order by n.dateCreate asc";       
		
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("name", name);
        
        return query.list();
	}
	public List<Notification> listNotification() {
		
		return this.sessionFactory.getCurrentSession().createQuery("from Notification").list();
	}

	public void remove(Notification name) {
		sessionFactory.getCurrentSession().delete(name);
	}
	
	
	
	public void addNotification(String inviter, String nameTopic, String user) {
			Notification notification = new Notification();
	        
			notification.setId(UUID.randomUUID().toString());        
			notification.setInviter(inviter);
			notification.setNameTopic(nameTopic);
			notification.setDateCreate(new Date());
			notification.setUser(user);	        
	        this.sessionFactory.getCurrentSession().persist(notification);		
	}
    
	public Notification findNotificationByID(String id) {
		Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Notification.class);
        crit.add(Restrictions.eq("id", id));
        return (Notification) crit.uniqueResult();
	}
	
	public Notification findNotificationByTopicNeme(String nameTopic) {
		Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Notification.class);
        crit.add(Restrictions.eq("nameTopic", nameTopic));
        return (Notification) crit.uniqueResult();
	}
	
	public Notification findNotificationByTopicNemeAndInviter(String inviter, String nameTopic) {
		Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Notification.class);
        crit.add(Restrictions.eq("nameTopic", nameTopic));
        crit.add(Restrictions.eq("inviter", inviter));
        return (Notification) crit.uniqueResult();
	}

	public NotificationInfo getNotificationInfo(String inviter, String nameTopic) {
		Notification notification = this.findNotificationByTopicNemeAndInviter(inviter, nameTopic);
        if (notification == null) {
            return null;
        }
        return new NotificationInfo(notification.getId(), inviter, nameTopic, notification.getDateCreate());
	}
	
	public void saveNotification(NotificationInfo notificationInfo) {
			String code = notificationInfo.getId();
			String inviter = notificationInfo.getInviter(); 
            Date date = notificationInfo.getDateCreate();
            String nameTopic = notificationInfo.getNameTopic(); 
            String user = notificationInfo.getUser();
			Notification notification = null;

	            notification = new Notification();
	            notification.setId(code);
	            notification.setInviter(inviter); 
	            notification.setDateCreate(date);
	            notification.setNameTopic(nameTopic);
	            notification.setUser(user);
	        
	        	        
	        this.sessionFactory.getCurrentSession().persist(notification);		
	}
	
	public void updateNotification(NotificationInfo notificationInfo) {
		//String cod = notificationInfo.;
		String nameTopic = notificationInfo.getNameTopic();
		String nameUser = notificationInfo.getUser();
		Notification notification = (Notification) this.sessionFactory.getCurrentSession().get(Notification.class, nameTopic);
		notification.setUser(nameUser);
		notification.setId(notification.getId());
        notification.setInviter(notification.getInviter()); 
        notification.setDateCreate(notification.getDateCreate());
        notification.setNameTopic(notification.getNameTopic());
        
		this.sessionFactory.getCurrentSession().save(notification);
	}

	public List<NotificationInfo> listmember(String topicName) {
		String sql = "Select new " + NotificationInfo.class.getName() //
				+ "(d.topic.id, d.topic.creatorUser, d.user.userName, d.topic.nameTopic) "//
                + " from " + TopicDetails.class.getName() + " d"
                + " where d.topic.nameTopic = :topicName"
                + " group by d.user.userName order by d.user.userName asc";       
 
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        query.setParameter("topicName",topicName);
        //query.setParameter("userName",userName);
        
        return query.list();
	}
	
	public List<NotificationInfo> listaccount() {
		String sql = "Select new " + NotificationInfo.class.getName() //
				+ "(a.userName) "//
                + " from " + Account.class.getName() + " a"//
                + " order by a.userName asc";       
 
        Session session = this.sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        
        return query.list();
	}
}
