package com.epam.thinkers.dao.impl;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import com.epam.thinkers.dao.AccountDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.PaginationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Query;
 
// Transactional for Hibernate
@Transactional
public class AccountDAOImpl implements AccountDAO {
    
    @Autowired
    private SessionFactory sessionFactory;
 
    public Account findAccount(String userName ) {
        Session session = sessionFactory.getCurrentSession();
        Criteria crit = session.createCriteria(Account.class);
        crit.add(Restrictions.eq("userName", userName));
        return (Account) crit.uniqueResult();
    }

	public AccountInfo findAccountInfo(String code) {
		Account account = this.findAccount(code);
        if (account == null) {
            return null;
        }
        return new AccountInfo(account.getUserName(), account.getPassword());
    }

	public void save(AccountInfo accountInfo) {
		String code = accountInfo.getName();
		 
        Account account = null;
 
        boolean isNew = false;
        if (code != null) {
        	account = this.findAccount(code);
        }
        if (account == null) {
        	//if(productInfo.getPassword().equals(productInfo.getPassword2()))
            isNew = true;
            account = new Account();            
        }
        account.setUserName(code);
        account.setUserRole(accountInfo.getUserRole());
        account.setPassword(accountInfo.getPassword());
        account.setActive(accountInfo.isActive());

        if (accountInfo.getFileData() != null) {
            byte[] image = accountInfo.getFileData().getBytes();
            if (image != null && image.length > 0) {
            	account.setPhoto(image);
            }
        }
        if (isNew) {
            this.sessionFactory.getCurrentSession().persist(account);
        }
        // If error in DB, Exceptions will be thrown out immediately
        this.sessionFactory.getCurrentSession().flush();
	}
	
	public List<Account> listMember() {
			return this.sessionFactory.getCurrentSession().createQuery("from Account").list();
	}
	
	public PaginationResult<AccountInfo> queryAccounts(int page, int maxResult, int maxNavigationPage,
			String likeName) {
		String sql = "Select new " + AccountInfo.class.getName() //
                + "(a.userName, a.password) " + " from "//
                + Account.class.getName() + " a ";
        if (likeName != null && likeName.length() > 0) {
            sql += " Where lower(a.userName) like :likeName ";
        }
        sql += " order by a.userName desc ";
        //
        Session session = sessionFactory.getCurrentSession();
 
        Query query = session.createQuery(sql);
        if (likeName != null && likeName.length() > 0) {
            query.setParameter("likeName", "%" + likeName.toLowerCase() + "%");
        }
        return new PaginationResult<AccountInfo>(query, page, maxResult, maxNavigationPage);
    }

	public PaginationResult<AccountInfo> queryAccounts(int page, int maxResult, int maxNavigationPage) {
		return queryAccounts(page, maxResult, maxNavigationPage, null);
	}
	
	
}
