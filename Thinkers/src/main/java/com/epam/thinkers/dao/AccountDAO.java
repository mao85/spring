package com.epam.thinkers.dao;


import java.util.List;

import com.epam.thinkers.entity.Account;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.PaginationResult;

public interface AccountDAO {
    
    public Account findAccount(String userName ); 
    public void save(AccountInfo productInfo);
    public AccountInfo findAccountInfo(String code) ;
    public PaginationResult<AccountInfo> queryAccounts(int page, int maxResult, int maxNavigationPage  );    
    public PaginationResult<AccountInfo> queryAccounts(int page, int maxResult, int maxNavigationPage, String likeName); 
    public List<Account> listMember(); 
}
