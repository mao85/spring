package com.epam.thinkers.dao;

import java.util.List;

import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.PostDetaisInfo;
import com.epam.thinkers.model.PostInfo;

public interface PostDAO {

	public PostInfo getPostInfo(String postId);

	public List<PostInfo> listPostByTopic(String topicId);
	public List<PostDetaisInfo> listPostDetailsByTopic(String topicID);

	public void edit(PostInfo postInfo);

	public PaginationResult<PostInfo> listPostByUser(int page, int maxResult, int maxNavigationPage, String userName);

	public void savePost(PostInfo postInfo);
	public PostInfo getPostInfoByUser(String creatorUser);
}
