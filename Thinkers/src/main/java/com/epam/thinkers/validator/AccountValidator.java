package com.epam.thinkers.validator;

import com.epam.thinkers.dao.AccountDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.model.AccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AccountValidator implements Validator{

	@Autowired
	private AccountDAO accountDAO;
	
	public boolean supports(Class<?> clazz) {
		return clazz == AccountInfo.class;
	}

	public void validate(Object target, Errors errors) {
		AccountInfo productInfo = (AccountInfo) target;
		 
        // Check the fields of ProductInfo class.
       // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "NotEmpty.productForm.code");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.productForm.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.productForm.password");
 
        String name = productInfo.getName();
        if(productInfo.isNewAccount()) {
                Account product = accountDAO.findAccount(name);
                if (product != null) {
                    errors.rejectValue("name", "Duplicate.productForm.name");
                }
                if(!productInfo.getPassword().equals(productInfo.getPassword2())){
                	 errors.rejectValue("password2", "Difference.productForm.password");
                }
            
        }
	}

}
