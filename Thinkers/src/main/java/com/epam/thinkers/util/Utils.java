package com.epam.thinkers.util;
import javax.servlet.http.HttpServletRequest;

import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.FullTopicInfo;
import com.epam.thinkers.model.TopicInfo;


 
public class Utils {
 
	// Products in Cart, stored in Session.
    public static FullTopicInfo getInSession(HttpServletRequest request) {
 
        // Get Cart from Session.
    	FullTopicInfo fullTopInfo = (FullTopicInfo) request.getSession().getAttribute("myCart");
        
        // If null, create it.
        if (fullTopInfo == null) {
        	fullTopInfo = new FullTopicInfo();
            
            // And store to Session.
            request.getSession().setAttribute("myCart", fullTopInfo);
        }
 
        return fullTopInfo;
    }
 
    public static void removeCartInSession(HttpServletRequest request) {
        request.getSession().removeAttribute("myCart");
    }
 
    public static void storeLastOrderedCartInSession(HttpServletRequest request, FullTopicInfo cartInfo) {
        request.getSession().setAttribute("lastOrderedCart", cartInfo);
    }
    
    public static FullTopicInfo getLastOrderedCartInSession(HttpServletRequest request) {
        return (FullTopicInfo) request.getSession().getAttribute("lastOrderedCart");
    }
}
