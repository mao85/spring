package com.epam.thinkers.controller;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import com.epam.thinkers.dao.AccountDAO;
import com.epam.thinkers.dao.PostDAO;
import com.epam.thinkers.dao.TopicDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.FullTopicInfo;
import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.PostDetaisInfo;
import com.epam.thinkers.model.PostInfo;
import com.epam.thinkers.model.TopicDetailInfo;
import com.epam.thinkers.model.TopicInfo;
import com.epam.thinkers.validator.AccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
 
@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class AdminController {
 
    
    // Configurated In ApplicationContextConfig.
	@Autowired
    private TopicDAO topicDAO;
	
	@Autowired
    private AccountDAO accountDAO;
	
	@Autowired
    private PostDAO postDAO;
 
	@Autowired
    private AccountValidator accountInfoValidator;
	
	@Autowired
    private ResourceBundleMessageSource messageSource;
	
	@InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        System.out.println("Target=" + target);
 
        if (target.getClass() == AccountInfo.class) {
            dataBinder.setValidator(accountInfoValidator);
            // For upload Image.
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
        else if (target.getClass() == PostInfo.class) {
        	
        }
    }
 
    // GET: Show Login Page
    @RequestMapping(value = { "/login" }, method = RequestMethod.GET)
    public String login(Model model) {
 
        return "login";
    }
 
    @RequestMapping(value = { "/accountInfo" }, method = RequestMethod.GET)
    public String accountInfo(Model model, TopicDetailInfo topic) {
 
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(userDetails.getPassword());
        System.out.println(userDetails.getUsername());
        System.out.println(userDetails.isEnabled());
 
        model.addAttribute("userDetails", userDetails);
        
        model.addAttribute("serachTopic", topic);
        return "accountInfo";
    }
    
    
    @RequestMapping(value = { "/topic" }, method = RequestMethod.GET)
    public String topicView(Model model, @RequestParam(value = "id", required=false) String topicId) {
 
    	TopicInfo topicInfo = null;
        if (topicId != null) {
        	topicInfo = this.topicDAO.getTopicInfo(topicId);
        }
        if (topicInfo == null) {
            return "redirect:/topicList";
        }
        List<TopicDetailInfo> details = this.topicDAO.listTopicDetailInfos(topicId);
        topicInfo.setListmembers(details);
 
        model.addAttribute("topicInfo", topicInfo);
        //************************
        
       
        List<PostInfo> paginationResult //
        = postDAO.listPostByTopic(topicId);
        
        model.addAttribute("paginationResult", paginationResult);
 
        return "topic";
    }
    
    @RequestMapping(value = { "/topicList" }, method = RequestMethod.GET)
    public String topicList(Model model, //
            @RequestParam(value = "page", defaultValue = "1") String pageStr, String name) {
    	 UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
         name = userDetails.getUsername();
    	int page = 1;
        try {
            page = Integer.parseInt(pageStr);
        } catch (Exception e) {
        }
        final int MAX_RESULT = 2;
        final int MAX_NAVIGATION_PAGE = 10;
        
        PaginationResult<TopicDetailInfo> paginationResult //
        = topicDAO.listTopicDetailUser(page, MAX_RESULT, MAX_NAVIGATION_PAGE, name);
        model.addAttribute("paginationResult", paginationResult);
        return "topicList";
    }
    
    @RequestMapping(value = { "/post" }, method = RequestMethod.GET)
    public String postView(Model model, @RequestParam("id") String postId) {
        PostInfo postInfo =  this.postDAO.getPostInfo(postId);
        
        List<PostDetaisInfo> details = this.postDAO.listPostDetailsByTopic(postId);
        postInfo.setPostDetails(details);
 
        model.addAttribute("postInfo", postInfo);
 
        return "post";
    }
    //----------------------------
  /*  @RequestMapping(value = { "/search" }, method = RequestMethod.GET)
    public String topicSearch(Model model, //
            @RequestParam(value = "page", defaultValue = "1") String pageStr, String name) {
    	 UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
         name = userDetails.getUsername();
    	int page = 1;
        try {
            page = Integer.parseInt(pageStr);
        } catch (Exception e) {
        }
        final int MAX_RESULT = 8;
        final int MAX_NAVIGATION_PAGE = 10;
        
        PaginationResult<TopicDetailInfo> paginationResult //
        = topicDAO.listTopicSearch(page, MAX_RESULT, MAX_NAVIGATION_PAGE, name);
        model.addAttribute("paginationResult", paginationResult);
        return "search";
    }
    */
    @RequestMapping(value = { "/search" })
    public String topicSearchByName(Model model,//
            @RequestParam(value = "page", defaultValue = "1") String pageStr, String name
    		) {
    	 UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
         name = userDetails.getUsername();
        
    	int page = 1;
        try {
            page = Integer.parseInt(pageStr);
        } catch (Exception e) {
        }
        final int MAX_RESULT = 8;
        final int MAX_NAVIGATION_PAGE = 10;
        
        PaginationResult<TopicDetailInfo> paginationResult //
        = topicDAO.listTopicSearch(page, MAX_RESULT, MAX_NAVIGATION_PAGE, name);
       
        
        PaginationResult<TopicDetailInfo> listTopicDetailUser = topicDAO.listTopicDetailUser(page, MAX_RESULT, MAX_NAVIGATION_PAGE, name);
        for (TopicDetailInfo list: paginationResult.getList()){
        	for (TopicDetailInfo sublist: listTopicDetailUser.getList()){
            	if(list.getTopicName().equals(sublist.getTopicName())){
            		list.setUserName(name);
            	}
            }
        }  
        
        model.addAttribute("paginationResult", paginationResult);
        
        return "search";
    }
    
    
    @RequestMapping(value = { "/searchTest" }, method = RequestMethod.GET)
    public String topicSearchByNameTest(Model model,//
            String name, 
            @RequestParam(value="topicName", required=false) 
            String topicName) {
    	 UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
         name = userDetails.getUsername();
        
        List<TopicDetailInfo> paginationResult = topicDAO.listTopicSearchTest(name, topicName);
       
        List<TopicDetailInfo> listTopicDetailUser = topicDAO.listTopicDetailUserByNeme(name);
        for (TopicDetailInfo list: paginationResult){
        	for (TopicDetailInfo sublist: listTopicDetailUser){
            	if(list.getTopicName().equals(sublist.getTopicName())){
            		list.setUserName(name);
            	}
            }
        }       
        
        model.addAttribute("paginationResultList", paginationResult);
        
        
        return "search";
    }
    //-----------------------------
  
    // GET: Show product.
    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public String account(Model model, @RequestParam(value = "name", defaultValue = "") String code) {
        AccountInfo accountInfo = null;
 
        if (code != null && code.length() > 0) {
        	accountInfo = accountDAO.findAccountInfo(code);
        }
        if (accountInfo == null) {
        	accountInfo = new AccountInfo();
        	accountInfo.setNewAccount(true);
        }
        model.addAttribute("accountForm", accountInfo);
        
        return "registration";
    }
 
    // POST: Save product
    @RequestMapping(value = { "/registration" }, method = RequestMethod.POST)
    // Avoid UnexpectedRollbackException (See more explanations)
    @Transactional(propagation = Propagation.NEVER)
    public String accountSave(Model model, //
            @ModelAttribute("accountForm") @Validated AccountInfo accountInfo, //
            BindingResult result, //
            final RedirectAttributes redirectAttributes) {
 
        if (result.hasErrors()) {
            return "registration";
        }
        try {        	
            accountDAO.save(accountInfo);
        } catch (Exception e) {
            // Need: Propagation.NEVER?
            String message = e.getMessage();
            model.addAttribute("message", message);
            // Show product form.
            return "registration";
 
        }
        return "redirect:/";
    }   
    
    @RequestMapping(value = {"/topicNew"}, method = RequestMethod.GET)
    public String topicNew(Model model, @RequestParam(value = "id", defaultValue = "") String code) {
    	
    	TopicInfo topicInfo = null;
 
        if (code != null && code.length() > 0) {
        	topicInfo = topicDAO.getTopicInfo(code);
        }
        if (topicInfo == null) {
        	topicInfo = new TopicInfo();        	
        	topicInfo.setNewTopic(true);
        }
        model.addAttribute("topic", topicInfo);
        
        return "topicNew";
    }
    
    @RequestMapping(value = { "/topicNew" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String topicSave(String name, Model model, @ModelAttribute("topic") TopicInfo topicInfo, BindingResult result, final RedirectAttributes redirectAttributes) {
    	
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
        
        if (result.hasErrors()) {
            return "topicNew";
        }
        try {  
        	//PostInfo postInfo = postDAO.getPostInfoByUser(name);
        	//topicInfo.addPost(postInfo);
        	AccountInfo accountInfo = accountDAO.findAccountInfo(name);
        	topicInfo.addUser(accountInfo);
        	topicInfo.setCreatorUser(name);
        	topicInfo.setId(UUID.randomUUID().toString());
        	topicDAO.saveTopic(topicInfo);        	
        } catch (Exception e) {
            String message = e.getMessage();
            model.addAttribute("message", message);
            return "topicNew";
        }
        
        return "topicC";
    }
    
    @RequestMapping(value = {"/topicEdit"}, method = RequestMethod.GET)
    public String topicEdits(Model model, @RequestParam(value = "id", required=true) String code) {
    	
        model.addAttribute("topic", topicDAO.getInfo(code));
        
        return "topicNew";
    }
    @RequestMapping(value = {"/topicMy"}, method = RequestMethod.GET)
    public String topicShow(Model model, @RequestParam(value = "id", required=false) String code,
    		@RequestParam(value = "page", defaultValue = "1") String pageStr) {
    	
    	TopicInfo topicInfo = topicDAO.getInfo(code);
    	model.addAttribute("topic", topicInfo);
    	
    	
       //String topicID = topicInfo.getId();
        List<PostInfo> paginationResult //
        = postDAO.listPostByTopic(code);
        
        model.addAttribute("paginationResult", paginationResult);
    	
   		return "topicC";     
    }
    
    
    @RequestMapping(value = { "/topicEdit" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String topicEdit(//String name, 
    		Model model, @ModelAttribute("topic") TopicInfo topicInfo, @RequestParam(value="id", required=true) String code) {
    	//UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        //name = userDetails.getUsername();
    	topicInfo.setId(code);
    	//topicInfo.setCreatorUser(name);
    	topicDAO.edit(topicInfo);
    	model.addAttribute("id", code);
        
    	List<PostInfo> paginationResult //
        = postDAO.listPostByTopic(code);
        
        model.addAttribute("paginationResult", paginationResult);
    	
    	
        return "topicC";
    }
    
    @RequestMapping(value = {"/postEdit"}, method = RequestMethod.GET)
    public String postEdits(Model model, @RequestParam(value = "id", required=true) String code) {
    	
        model.addAttribute("postInfo", postDAO.getPostInfo(code));
        
        return "postNew";
    }
    
    @RequestMapping(value = { "/postEdit" }, method = RequestMethod.POST)
    //@Transactional(propagation = Propagation.NEVER)
    public String postEdit(String name, 
    		Model model, @ModelAttribute("postInfo") PostInfo postInfo, @RequestParam(value="id", required=true) String code) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
       // postInfo.setPostDate(new Date());
    	//Topic topic = this.topicDAO.findTopicForName(postInfo.getTopicName());
    	postInfo.setId(code);
    	//postInfo.setTopicID(topic.getId());
    	postInfo.setPostUser(name);
    	//topicInfo.setCreatorUser(name);
    	postDAO.edit(postInfo);
    	model.addAttribute("id", code);
        
        return "post";
    }
    @RequestMapping(value = { "/feed" }, method = RequestMethod.GET)
    public String feed(Model model, //
            @RequestParam(value = "page", defaultValue = "1") String pageStr, String userName) {
    	 UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
    	 userName = userDetails.getUsername();
    	int page = 1;
        try {
            page = Integer.parseInt(pageStr);
        } catch (Exception e) {
        }
        final int MAX_RESULT = 8;
        final int MAX_NAVIGATION_PAGE = 10;
        
        PaginationResult<PostInfo> paginationResult //
        = postDAO.listPostByUser(page, MAX_RESULT, MAX_NAVIGATION_PAGE, userName);
        model.addAttribute("paginationResult", paginationResult);
        return "feed";
    }
    
    @RequestMapping(value = {"/newPost"}, method = RequestMethod.GET)
    public String postNew(Model model, @RequestParam(value = "id") String code) {
    	
    	TopicInfo topicInfo = topicDAO.getTopicInfo(code);
    	
        PostInfo postInfo = new PostInfo();        	
        	
        model.addAttribute("postInfo", postInfo);
        model.addAttribute("topic", topicInfo);
        
        return "postNew";
    }
    
    @RequestMapping(value = { "/newPost" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String postSave(String name, Model model, @RequestParam(value = "id", defaultValue = "") String code,
    		@ModelAttribute("topic") TopicInfo topicInfo, @ModelAttribute("postInfo") PostInfo postInfo, BindingResult result, final RedirectAttributes redirectAttributes) {
    	
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
        
        if (result.hasErrors()) {
            return "postNew";
        }
        try {  
        	//AccountInfo accountInfo = accountDAO.findAccountInfo(name);
        	//topicInfo.addUser(accountInfo);        	
        	topicInfo.addPost(postInfo);
        	postInfo.setTopicID(topicInfo.getId());
        	postInfo.setPostUser(name);
        	postInfo.setId(UUID.randomUUID().toString());
        	postInfo.setPostDate(new Date());
        	postDAO.savePost(postInfo);        	
        } catch (Exception e) {
            String message = e.getMessage();
            model.addAttribute("message", message);
            return "postNew";
        }
        
        return "post";
    }
    
}
