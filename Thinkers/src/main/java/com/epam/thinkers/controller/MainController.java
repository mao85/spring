package com.epam.thinkers.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.thinkers.dao.AccountDAO;
import com.epam.thinkers.dao.NotificationDAO;
import com.epam.thinkers.dao.PostDAO;
import com.epam.thinkers.dao.TopicDAO;
import com.epam.thinkers.entity.Account;
import com.epam.thinkers.entity.Notification;
import com.epam.thinkers.entity.Topic;
import com.epam.thinkers.entity.TopicDetails;
import com.epam.thinkers.model.AccountInfo;
import com.epam.thinkers.model.FullTopicInfo;
import com.epam.thinkers.model.NotificationInfo;
import com.epam.thinkers.model.PaginationResult;
import com.epam.thinkers.model.PostInfo;
import com.epam.thinkers.model.TopicDetailInfo;
import com.epam.thinkers.model.TopicInfo;
import com.epam.thinkers.util.Utils;
import com.epam.thinkers.validator.AccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
 
@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class MainController {
 
	@Autowired
    private TopicDAO topicDAO;
	
	@Autowired
    private AccountDAO accountDAO;
 
	@Autowired
    private NotificationDAO notificationDAO;
	
	@Autowired
    private AccountValidator accountInfoValidator;
	
	 @InitBinder
	    public void myInitBinder(WebDataBinder dataBinder) {
	        Object target = dataBinder.getTarget();
	        if (target == null) {
	            return;
	        }
	        System.out.println("Target=" + target);
	 
	        // For Cart Form.
	        // (@ModelAttribute("cartForm") @Validated CartInfo cartForm)
	        if (target.getClass() == FullTopicInfo.class) {
	 
	        }
	        // For Customer Form.
	        // (@ModelAttribute("customerForm") @Validated CustomerInfo
	        // customerForm)
	        else if (target.getClass() == AccountInfo.class) {
	        	dataBinder.setValidator(accountInfoValidator);
	        }
	        else if (target.getClass() == NotificationInfo.class) {
	        	//dataBinder.setValidator(accountInfoValidator);
	        }
	 
	    }
	
	@RequestMapping("/403")
    public String accessDenied() {
        return "/403";
    }
 
    @RequestMapping("/")
    public String home() {
        return "index";
    }
    
	
	
    @RequestMapping({ "/remove" })
    public String remove(HttpServletRequest request, Model model, //
            @RequestParam(value = "cod", defaultValue = "") String code, String name) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
    	List<TopicDetails> topicDetails = topicDAO.listTopicDetails();
    	for(TopicDetails list: topicDetails){
    	if(list.getUser().getUserName().equals(name) && 
    			list.getTopic().getNameTopic().equals(code))
    		topicDAO.remove(list);
    	}
        return "redirect:/topicList";
    }

  
    @RequestMapping({ "/LeaveTopic" })
    public String LeaveTopic(HttpServletRequest request, Model model, //
            @RequestParam(value = "cod", defaultValue = "") String code, String name) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
    	List<TopicDetails> topicDetails = topicDAO.listTopicDetails();
    	for(TopicDetails list: topicDetails){
    	if(	list.getUser().getUserName().equals(name) && 
    			list.getTopic().getNameTopic().equals(code))
    		topicDAO.remove(list);
    	}
        return "redirect:/searchTest?topicName="+code;
    }
    
    @RequestMapping({ "/addTopic" })
    public String addTopicDetails(HttpServletRequest request, Model model, @RequestParam(value = "nameTopic", defaultValue = "") String nameTopic, String name) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
    	//List<TopicDetails> topicDetails = topicDAO.listTopicDetails();
    	TopicDetailInfo topicDetailInfo = topicDAO.findTopicDetailInfo(nameTopic, name);
    	//for(TopicDetails list: topicDetails){
    	//if(list.getUser().getUserName().equals(name) && 
    	//		list.getTopic().getNameTopic().equals(code))
    		topicDAO.addDetails(topicDetailInfo);
    	//}
        return "redirect:/searchTest?topicName="+nameTopic;
    }
    
    @RequestMapping(value = { "/image" }, method = RequestMethod.GET)
    public void loadImage(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam("name") String name) throws IOException {
        Account account = null;
        if (name != null) {
        	account = this.accountDAO.findAccount(name);
        }
        if (account != null && account.getPhoto() != null) {
            response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
            response.getOutputStream().write(account.getPhoto());
        }
        response.getOutputStream().close();
    }
    
    
    @RequestMapping(value = { "/notification" }, method = RequestMethod.GET)
    public String notification(Model model, String name, String id) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
        List<Notification> notification = notificationDAO.listNotification();
    	
        model.addAttribute("notification", notification);
        model.addAttribute("id", id);
        return "invite";
    }
    
    @RequestMapping(value = { "/notificationMy" }, method = RequestMethod.GET)
    public String notificationMy(Model model, String name, String id) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
        List<Notification> notification = notificationDAO.listNotification();
     	
        model.addAttribute("notification", notification);
        model.addAttribute("id", id);
        return "inviteMy";
    }
    
    @RequestMapping({ "/cancelInvite" })
    public String noInvite(HttpServletRequest request, Model model,
            @RequestParam(value = "id") String code
           // , String name
            ) {
    	//UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        //name = userDetails.getUsername();
    	List<Notification> notification = notificationDAO.listNotification();
    	Notification not = notificationDAO.findNotificationByID(code);
    	notificationDAO.remove(not);
    	model.addAttribute("notification", notification);
        return "redirect:/notification";
    }
    
    @RequestMapping({ "/yesInvite" })
    public String yesInvite(HttpServletRequest request, Model model, @RequestParam(value = "nameTopic", defaultValue = "") String nameTopic, String name) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
    	TopicDetailInfo topicDetailInfo = topicDAO.findTopicDetailInfo(nameTopic, name);
    	topicDAO.addDetails(topicDetailInfo);
    	
    	List<Notification> notification = notificationDAO.listNotification();
    	
    	model.addAttribute("notification", notification);
    	model.addAttribute("messageYes", "cancel");
    	    		
        return "redirect:/notification";
    }
    
    
    @RequestMapping(value = { "/collInvite" })
    public String collInvite(HttpServletRequest request, Model model, @RequestParam(value = "nameTopic") String nameTopic, String userName,  String name) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
      
        List<NotificationInfo> listaccount = notificationDAO.listaccount();
       
        for(int i=1; i<listaccount.size(); i++){
        	notificationDAO.saveNotification(new NotificationInfo(name, listaccount.get(i).getUser(), nameTopic));       	
        }
        
        TopicInfo topicInfo = topicDAO.findTopicInfoByName(nameTopic);
    	model.addAttribute("topic", topicInfo);
    	model.addAttribute("messageInvite", "message is sent");
    	model.addAttribute("messageModify", "Modify Notification");
        return "topicC";
    }
    
   /* @RequestMapping(value = { "/addInvite" })
    //@Transactional(propagation = Propagation.NEVER)
    public String addInvite(HttpServletRequest request, Model model, String nameTopic, @RequestParam(value = "userName") String userName,
    		@ModelAttribute("notification")	NotificationInfo notification, String name) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();         
        name = userDetails.getUsername();
        notification.setId(UUID.randomUUID().toString());
        notification.setInviter(name);       
        notification.setDateCreate(new Date());
        //notification.setNameTopic(nameTopic);
        notification.setUser(userName);
        notificationDAO.updateNotification(notification);
          
        model.addAttribute("nameTopic", nameTopic);
        model.addAttribute("userName", userName);
        
        return "topicC";
    }*/
    
}
