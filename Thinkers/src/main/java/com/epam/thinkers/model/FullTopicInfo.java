package com.epam.thinkers.model;

import java.util.ArrayList;
import java.util.List;

public class FullTopicInfo {
	private String userCreator;
	private String nameTopic;
	private boolean flag;
    private String description;
	private AccountInfo accountInfo;
	private final List<AccountInfo> userLines = new ArrayList<AccountInfo>();
	//private final List<PostLineInfo> postLines = new ArrayList<PostLineInfo>();
	
	public FullTopicInfo() {
		}
	
	public FullTopicInfo(String userCreator, String nameTopic, boolean flag, String description,
			AccountInfo accountInfo) {
		super();
		this.userCreator = userCreator;
		this.nameTopic = nameTopic;
		this.flag = flag;
		this.description = description;
		this.accountInfo = accountInfo;
	}

	public AccountInfo getAccountInfo() {
		return accountInfo;
	}
	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public String getNameTopic() {
		return nameTopic;
	}

	public void setNameTopic(String nameTopic) {
		this.nameTopic = nameTopic;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getUserCreator() {
		return userCreator;
	}

	public void setUserCreator(String userCreator) {
		this.userCreator = userCreator;
	}

	public List<AccountInfo> getUserLines() {
		return userLines;
	}
	
	private AccountInfo findUserLineById(String name) {
        for (AccountInfo line : this.userLines) {
            if (line.getName().equals(name)) {
                return line;
            }
        }
        return null;
    }
 
    public void addUser(AccountInfo accountInfo) {
    	
            this.userLines.add(accountInfo); 
    	 
    }
 
    
    public void validate() {
    	 
    }
    public void removeUser(String name) { 
    	AccountInfo line = this.findUserLineById(name);
    	if (line != null) {
            this.userLines.remove(line);            
        }   
    }
 
    public boolean isEmptyUsers() {
        return this.userLines.isEmpty();
    }
    public boolean isValidAccountInfo() {
        return this.accountInfo != null && this.accountInfo.isValid();
    }
	
   
	
}
