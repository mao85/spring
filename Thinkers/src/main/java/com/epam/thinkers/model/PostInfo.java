package com.epam.thinkers.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.epam.thinkers.entity.Post;

public class PostInfo {

	private String id;
	private String postName;
	private String postDescription;
	private String postUser;
	private Date postDate;
	private String topicID;	
	private String topicName;	
	private String topicDescription;	
	private String topicUser;		
	private boolean topicFlag;
	
	private AccountInfo accountInfo;
	private final List<AccountInfo> userLines = new ArrayList<AccountInfo>();	
	private List<PostDetaisInfo> postDetails;
	private boolean newPost=false;
	
	public PostInfo() {

	}

	public PostInfo(String id, String postName, Date postDate, String postUser, String postDescription, String topicName, String topicID){
		this.id = id;
		this.postName = postName;
		this.postDate = postDate;
		this.postUser = postUser;
		this.postDescription = postDescription;	
		this.topicName = topicName;
		this.topicID = topicID;
	}
	public PostInfo(String id, String name, String postDescription, String postUser, String topicName,
			String topicDescription, String topicUser, boolean topicFlag) {
		this.id = id;
		this.postName = name;
		this.postDescription = postDescription;
		this.postUser = postUser;
		this.topicName = topicName;
		this.topicDescription = topicDescription;
		this.topicUser = topicUser;
		this.topicFlag = topicFlag;
	}

	
	
	
	public List<PostDetaisInfo> getPostDetails() {
		return postDetails;
	}

	public void setPostDetails(List<PostDetaisInfo> postDetails) {
		this.postDetails = postDetails;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String name) {
		this.postName = name;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public String getPostDescription() {
		return postDescription;
	}

	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}

	public String getPostUser() {
		return postUser;
	}

	public void setPostUser(String postUser) {
		this.postUser = postUser;
	}

	public String getTopicID() {
		return topicID;
	}

	public void setTopicID(String topicID) {
		this.topicID = topicID;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicDescription() {
		return topicDescription;
	}

	public void setTopicDescription(String topicDescription) {
		this.topicDescription = topicDescription;
	}

	public String getTopicUser() {
		return topicUser;
	}

	public void setTopicUser(String topicUser) {
		this.topicUser = topicUser;
	}

	public boolean isTopicFlag() {
		return topicFlag;
	}

	public void setTopicFlag(boolean topicFlag) {
		this.topicFlag = topicFlag;
	}

	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public boolean isNewPost() {
		return newPost;
	}

	public void setNewPost(boolean newPost) {
		this.newPost = newPost;
	}

	public List<AccountInfo> getUserLines() {
		return userLines;
	}

	private AccountInfo findUserLineById(String name) {
        for (AccountInfo line : this.userLines) {
            if (line.getName().equals(name)) {
                return line;
            }
        }
        return null;
    }
 
    public void addUser(AccountInfo accountInfo) {
    	
            this.userLines.add(accountInfo);    	 
    }
    
    public void validate() {
    	 
    }
    public void removeUser(String name) { 
    	AccountInfo line = this.findUserLineById(name);
    	if (line != null) {
            this.userLines.remove(line);            
        }   
    }
 
    public boolean isEmptyUsers() {
        return this.userLines.isEmpty();
    }
    public boolean isValidAccountInfo() {
        return this.accountInfo != null && this.accountInfo.isValid();
    }
    
	
}
