package com.epam.thinkers.model;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.epam.thinkers.entity.Account;

public class AccountInfo {
	
	private String name;
	private String password;
	private String userRole = "USER";
	private boolean active = true;
	private String password2;
	
	private CommonsMultipartFile fileData;
	private boolean newAccount=false;
	private boolean valid;
	
	public AccountInfo() {
		
	}
	public AccountInfo(Account account) {
		this.name = account.getUserName();
		//this.userRole = account.getUserRole();
		this.password = account.getPassword();
		this.userRole = account.getUserRole();
		this.active = account.isActive();
	}
	
	public AccountInfo(String name, String password) {
		this.name = name;		
		//this.userRole = userRole;
		this.password = password;
		
	}
	
	public AccountInfo(String name) {
		this.name = name;		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isNewAccount() {
		return newAccount;
	}
	public void setNewAccount(boolean newAccount) {
		this.newAccount = newAccount;
	}
	public CommonsMultipartFile getFileData() {
		return fileData;
	}
	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isValid() {
	        return valid;
	    }
	 
	public void setValid(boolean valid) {
	        this.valid = valid;
	    }
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
		
}
