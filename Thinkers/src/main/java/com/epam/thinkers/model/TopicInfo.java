package com.epam.thinkers.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.epam.thinkers.entity.Topic;

public class TopicInfo {
	private String id;
	private String name;
	private String description;
	private String creatorUser;
	private boolean flag;
	
	private AccountInfo accountInfo;
	private final List<AccountInfo> userLines = new ArrayList<AccountInfo>();
	
	private PostInfo postInfo;
	private final List<PostInfo> postLines = new ArrayList<PostInfo>();
	
	
	private List<TopicDetailInfo> listmembers;
	private boolean newTopic=false;
	//private CommonsMultipartFile fileData;
	
	public TopicInfo() {

	}
	
	public TopicInfo(String id, String name, String description, String creatorUser, boolean flag) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.creatorUser = creatorUser;
		this.flag = flag;
	}

	
	public TopicInfo(String id, String name, String description, String creatorUser, boolean flag,
			AccountInfo accountInfo) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.creatorUser = creatorUser;
		this.flag = flag;
		this.accountInfo = accountInfo;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatorUser() {
		return creatorUser;
	}

	public void setCreatorUser(String creatorUser) {
		this.creatorUser = creatorUser;
	}

	public List<TopicDetailInfo> getListmembers() {
		return listmembers;
	}

	public void setListmembers(List<TopicDetailInfo> listmembers) {
		this.listmembers = listmembers;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public boolean isNewTopic() {
		return newTopic;
	}

	public void setNewTopic(boolean newTopic) {
		this.newTopic = newTopic;
	}

	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public List<AccountInfo> getUserLines() {
		return userLines;
	}

	private AccountInfo findUserLineById(String name) {
        for (AccountInfo line : this.userLines) {
            if (line.getName().equals(name)) {
                return line;
            }
        }
        return null;
    }
 
    public void addUser(AccountInfo accountInfo) {
    	
            this.userLines.add(accountInfo);    	 
    }
    
    public void validate() {
    	 
    }
    public void removeUser(String name) { 
    	AccountInfo line = this.findUserLineById(name);
    	if (line != null) {
            this.userLines.remove(line);            
        }   
    }
 
    public boolean isEmptyUsers() {
        return this.userLines.isEmpty();
    }
    public boolean isValidAccountInfo() {
        return this.accountInfo != null && this.accountInfo.isValid();
    }

	public PostInfo getPostInfo() {
		return postInfo;
	}

	public void setPostInfo(PostInfo postInfo) {
		this.postInfo = postInfo;
	}

	public List<PostInfo> getPostLines() {
		return postLines;
	}
	
	private PostInfo findPostLineById(String id) {
        for (PostInfo line : this.postLines) {
            if (line.getId().equals(id)) {
                return line;
            }
        }
        return null;
    }
 
    public void addPost(PostInfo postInfo) {
    	
            this.postLines.add(postInfo);    	 
    }
    
    public void removePost(String id) { 
    	PostInfo line = this.findPostLineById(id);
    	if (line != null) {
            this.postLines.remove(line);            
        }   
    }
 
    public boolean isEmptyPost() {
        return this.postLines.isEmpty();
    }
    
	
}
