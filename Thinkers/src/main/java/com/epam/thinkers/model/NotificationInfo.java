package com.epam.thinkers.model;

import java.util.Date;
import java.util.UUID;

public class NotificationInfo {
	private String id;
	private String inviter;
    private String user;
    private String nameTopic;
    private Date dateCreate;
	public NotificationInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NotificationInfo(String inviter, String nameTopic) {
		this.inviter = inviter;
		this.nameTopic = nameTopic;
	}
	
	
	public NotificationInfo(String id, String inviter, String nameTopic, Date dateCreate) {
		super();
		this.id = id;
		this.inviter = inviter;
		this.nameTopic = nameTopic;
		this.dateCreate = dateCreate;		
	}
	
	public NotificationInfo(String user) {		
		this.user = user;
	}	
	
	public NotificationInfo(String inviter, String user, String nameTopic) {
		this.id = UUID.randomUUID().toString();
		this.inviter = inviter;
		this.user = user;
		this.nameTopic = nameTopic;
		this.dateCreate = new Date();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInviter() {
		return inviter;
	}
	public void setInviter(String inviter) {
		this.inviter = inviter;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getNameTopic() {
		return nameTopic;
	}
	public void setNameTopic(String nameTopic) {
		this.nameTopic = nameTopic;
	}
	public Date getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}
    
}
