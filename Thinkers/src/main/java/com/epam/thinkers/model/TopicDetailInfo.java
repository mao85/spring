package com.epam.thinkers.model;

public class TopicDetailInfo implements Comparable {

	private String id;
	
	private String userName;
	
	private String topicName;	
	private String description;	
	private String creatorUser;	
	
	private boolean flag;
	
	public TopicDetailInfo(String id, String userName, String topicName,  String description, String creatorUser) {
		this.id = id;
		this.userName = userName;
		this.topicName = topicName;
		this.description = description;
		this.creatorUser = creatorUser;
	}
	
	public TopicDetailInfo(String id, String userName, String topicName,  String description, String creatorUser, boolean flag) {
		this.id = id;
		this.userName = userName;
		this.topicName = topicName;
		this.description = description;
		this.creatorUser = creatorUser;
		this.flag = flag;
	}
	
	public TopicDetailInfo(String id, String topicName,  String description, String creatorUser) {
		this.id = id;		
		this.topicName = topicName;
		this.description = description;
		this.creatorUser = creatorUser;
	}
	public TopicDetailInfo(String id, String topicName,  String description, String creatorUser, boolean flag) {
		this.id = id;		
		this.topicName = topicName;
		this.description = description;
		this.creatorUser = creatorUser;
		this.flag = flag;
	}
	public TopicDetailInfo() {
		
		
	}

	public TopicDetailInfo(String id, String userName, String topicName, boolean flag) {
		this.id = id;
		this.userName = userName;
		this.topicName = topicName;
		this.flag = flag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatorUser() {
		return creatorUser;
	}

	public void setCreatorUser(String creatorUser) {
		this.creatorUser = creatorUser;
	}

	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
}
